#!/bin/bash

# Before running/editing this script, please look at the documentation.


# WHAT THIS SCRIPT DOES:
# THIS SCRIPT WILL ALLOW THE USER TO TEST THE POWER CONSUMPTION OF ANDROID APPLICATIONS. 
# IT MAKES USE OF AN ANDROID EVENT RECORDER CALLED RERAN AND POWER TESTING HARDWARE. 
# EACH OF THE ANDROID APPLICATIONS CAN HAVE MULTIPLE VERSIONS (EACH VERSION NEEDS TO HAVE ITS OWN APK) 
# AND MULTIPLE USAGE SCENARIOS. THE SCRIPT WILL MEASURE THE POWER CONSUMPTION OF EACH VERSION OF EACH 
# APPLICATION RUNNING EACH USAGE SCENARIO FOR THE GIVEN NUMBER OF TRIALS.

# THE NUMBER OF TRIALS TO RUN TOTAL (INCLUDING THE CORRECTION VALUE)
TRIAL_COUNT=30

# IN THE CASE THAT THE PHONE LOSES POWER OR ADB CONNECTION (WHICH IS VERY COMMON), 
# THE CURRENT TRIAL WILL FAIL. TO PRESERVE CORRECT NUMBERING, CORRECTION_VAL SHOULD BE 
# THE NUMBER OF TRIALS ALREADY COMPLETED SUCCESSFULLY.
CORRECTION_VAL=0

# UPDATE THE FOLLOWING ACCORDINGLY

# PATH TO THE ROOT DIRECTORY OF THE PROJECT
PROJ=/home/green/Projects/AnEnergyEx

# PATH TO APPLICATION APKS
APKS=$PROJ/backend/app_apks

# PATH TO SCENARIO REPLAYS
REPLAY_PREFIX=$PROJ/backend/replays

# PATH TO RERAN FILES
RERAN=$PROJ/backend/scripts/misc/RERAN_Modifications

# PATH TO ANDROID SDK
ANDROID=/home/green/Android/Sdk

# PATH TO ADB
ADB="adb"

# PATH TO SCRIPT THAT WILL GATHER ENERGY DATA WHILE RUNNING SCENARIO
RUN_TRIAL=$PROJ/backend/scripts/performance-run-replay.py
#-------------------------------------------------------------------------------

# REPLAYS: PATH TO SCENARIOS' RECORDED EVENTS (REPLAY_PREFIX + document name), APP NAME, APP SCENARIO'S NAME
# EXAMPLE ENTRY:
# REPLAYS=$REPLAY_PREFIX"/usage_scenario-recordedEvents.txt,application,usage_scenario"

# NOTE: THERE SHOULD NOT BE ANY SPACES AFTER COMMAS IN THE STRING. IF THERE ARE THEN THE SCRIPT WILL NOT WORK.
# NOTE: ENSURE CAPITILIZATION IS CONSISTENT EVERYWHERE.

REPLAYS=""

# EBookDroid
#REPLAYS=$REPLAY_PREFIX"/open_pdf-recordedEvents.txt,EBookDroid,open_pdf"

# Shortyz
#REPLAYS=$REPLAY_PREFIX"/download_puzzles-recordedEvents.txt,Shortyz,download_puzzles"

# AnkiDroid
#REPLAYS=$REPLAY_PREFIX"/learn_cards-recordedEvents.txt,AnkiDroid,learn_cards"
#REPLAYS=$REPLAY_PREFIX"/make_deck-recordedEvents.txt,AnkiDroid,make_deck"

# ConnectBot
#REPLAYS=$REPLAY_PREFIX"/connect_ssh-recordedEvents.txt,ConnectBot,connect_ssh"
#REPLAYS=$REPLAY_PREFIX"/ssh_shell-recordedEvents.txt,ConnectBot,ssh_shell"

# Financius
#REPLAYS=$REPLAY_PREFIX"/write_transactions-recordedEvents.txt,Financius,write_transactions"

# SimpleCalendar
#REPLAYS=$REPLAY_PREFIX"/make_events-recordedEvents.txt,SimpleCalendar,make_events"

# AnExplorer
REPLAYS=$REPLAY_PREFIX"/explore_files-recordedEvents.txt,AnExplorer,explore_files"

# Minetest
#REPLAYS=$REPLAY_PREFIX"/play_game-recordedEvents.txt,Minetest,play_game"

# Materialistic
#REPLAYS=$REPLAY_PREFIX"/read_article-recordedEvents.txt,Materialistic,read_article"

#-------------------------------------------------------------------------------

# UPDATE ACCORDINGLY: VERSIONS REFER TO VERSIONS OF APKS FOR SUBJECT APPS
# EXAMPLE ENTRY:
#	VERSIONS[#] = "versionID"

# EBookDroid - open_pdf
#VERSIONS[0]="1.1.0_b_m"
#VERSIONS[0]="1.1.2_b_m"
#VERSIONS[0]="1.1.4_f_m"
#VERSIONS[0]="1.2.0_f"

# Shortyz - download_puzzles
#VERSIONS[0]="4.0.0_b"
#VERSIONS[0]="4.0.2_b_m"
#VERSIONS[0]="4.0.5_b_m"
#VERSIONS[0]="4.0.7_f"

# AnkiDroid - learn_cards, make_deck
#VERSIONS[0]="2.5_b"
#VERSIONS[0]="2.6_b"
#VERSIONS[0]="2.7_f"
#VERSIONS[0]="2.8_f"

# ConnectBot - connect_ssh
#VERSIONS[0]="1.8.3_b"
#VERSIONS[1]="1.8.6_b"
#VERSIONS[2]="1.8.6_f_m"
#VERSIONS[3]="1.9.0a2_f"

# ConnectBot - ssh_shell
#VERSIONS[0]="1.8.3"
#VERSIONS[0]="1.8.4"
#VERSIONS[0]="1.8.5"
#VERSIONS[0]="1.8.6"

# Financius - write_transaction
#VERSIONS[0]="0.12.1"
#VERSIONS[1]="0.13.0"
#VERSIONS[2]="0.14.0"
#VERSIONS[3]="0.15.0"

# SimpleCalendar - make_events
#VERSIONS[0]="8003feb"
#VERSIONS[0]="5379244"
#VERSIONS[0]="b2fe795"
#VERSIONS[0]="fd2ec79"

# AnExplorer - explore_files
#VERSIONS[0]="e7e51f8"
#VERSIONS[0]="419112b"
#VERSIONS[0]="53086f3"
VERSIONS[0]="d9a7443"

# Minetest - play_game
#VERSIONS[0]="0.4.12"
#VERSIONS[1]="0.4.13"
#VERSIONS[2]="0.4.14"
#VERSIONS[3]="0.4.15"

# Materialistic - read_article
#VERSIONS[0]="57"
#VERSIONS[0]="58"
#VERSIONS[0]="59"
#VERSIONS[0]="60"

 #-------------------------------------------------------------------------------

# UPDATE ACCORDINGLY: APKNAMES REFER TO FILENAMES OF APKS FOR EACH VERSION. SHOULD BE MADE CORRESPONDING TO VERSIONS
# EXAMPLE ENTRY:
#	APKNAMES[#] = "apk_name"

# EBookDroid - open_pdf
#APKNAMES[0]="EBookDroid-1.1.0_b_m.apk"
#APKNAMES[0]="EBookDroid-1.1.2_b_m.apk"
#APKNAMES[0]="EBookDroid-1.1.4_f_m.apk"
#APKNAMES[0]="EBookDroid-1.2.0_f.apk"

# Shortyz - download_puzzles
#APKNAMES[0]="Shortyz-4.0.0_b.apk"
#APKNAMES[0]="Shortyz-4.0.2_b_m.apk"
#APKNAMES[0]="Shortyz-4.0.5_b_m.apk"
#APKNAMES[0]="Shortyz-4.0.7_f.apk"

# AnkiDroid - learn_cards, make_deck
#APKNAMES[0]="AnkiDroid-2.5_b.apk"
#APKNAMES[0]="AnkiDroid-2.6_b.apk"
#APKNAMES[0]="AnkiDroid-2.7_f.apk"
#APKNAMES[0]="AnkiDroid-2.8_f.apk"

# ConnectBot - connect_ssh
#APKNAMES[0]="ConnectBot-1.8.3_b.apk"
#APKNAMES[1]="ConnectBot-1.8.6_b.apk"
#APKNAMES[2]="ConnectBot-1.8.6_f_m.apk"
#APKNAMES[3]="ConnectBot-1.9.0a2_f.apk"

# ConnectBot - ssh_shell
#APKNAMES[0]="ConnectBot-1.8.3.apk"
#APKNAMES[0]="ConnectBot-1.8.4.apk"
#APKNAMES[0]="ConnectBot-1.8.5.apk"
#APKNAMES[0]="ConnectBot-1.8.6.apk"

# Financius - write_transactions
#APKNAMES[0]="Financius-0.12.1.apk"
#APKNAMES[1]="Financius-0.13.0.apk"
#APKNAMES[2]="Financius-0.14.0.apk"
#APKNAMES[3]="Financius-0.15.0.apk"

# SimpleCalendar - make_events
#APKNAMES[0]="SimpleCalendar-8003feb.apk"
#APKNAMES[0]="SimpleCalendar-5379244.apk"
#APKNAMES[0]="SimpleCalendar-b2fe795.apk"
#APKNAMES[0]="SimpleCalendar-fd2ec79.apk"

# AnExplorer - explore_files
#APKNAMES[0]="AnExplorer-e7e51f8.apk"
#APKNAMES[0]="AnExplorer-419112b.apk"
#APKNAMES[0]="AnExplorer-53086f3.apk"
APKNAMES[0]="AnExplorer-d9a7443.apk"

# Minetest - play_game
#APKNAMES[0]="Minetest-0.4.12.apk"
#APKNAMES[1]="Minetest-0.4.13.apk"
#APKNAMES[2]="Minetest-0.4.14.apk"
#APKNAMES[3]="Minetest-0.4.15.apk"

# Materialistic - read_article
#APKNAMES[0]="Materialistic-57.apk"
#APKNAMES[0]="Materialistic-58.apk"
#APKNAMES[0]="Materialistic-59.apk"
#APKNAMES[0]="Materialistic-60.apk"

# -------------------------------------------------------------------------------

# UPDATE ACCORDINGLY: APKPACKAGES REFER TO PACKAGES OF APKS FOR EACH VERSION. SHOULD BE MADE CORRESPONDING TO VERSIONS

# EBookDroid - open_pdf
#APKPACKAGES[0]="org.ebookdroid"
#APKPACKAGES[0]="org.ebookdroid"
#APKPACKAGES[2]="org.ebookdroid"
#APKPACKAGES[0]="org.ebookdroid"

# Shortyz - download_puzzles
#APKPACKAGES[0]="com.totsp.crossword.shortyz"
#APKPACKAGES[0]="com.totsp.crossword.shortyz"
#APKPACKAGES[0]="com.totsp.crossword.shortyz"
#APKPACKAGES[0]="com.totsp.crossword.shortyz"

# AnkiDroid - learn_cards, make_deck
#APKPACKAGES[0]="com.ichi2.anki"
#APKPACKAGES[0]="com.ichi2.anki"
#APKPACKAGES[0]="com.ichi2.anki"
#APKPACKAGES[3]="com.ichi2.anki"

# ConnectBot - connect_ssh
#APKPACKAGES[0]="org.connectbot"
#APKPACKAGES[1]="org.connectbot"
#APKPACKAGES[2]="org.connectbot.debug"
#APKPACKAGES[3]="org.connectbot"

# ConnectBot - ssh_shell
#APKPACKAGES[0]="org.connectbot"
#APKPACKAGES[1]="org.connectbot"
#APKPACKAGES[2]="org.connectbot"
#APKPACKAGES[3]="org.connectbot"

# Financius - write_transactions
#APKPACKAGES[0]="com.code44.finance"
#APKPACKAGES[1]="com.code44.finance"
#APKPACKAGES[2]="com.code44.finance"
#APKPACKAGES[3]="com.code44.finance"

# SimpleCalendar - make_events
#APKPACKAGES[0]="com.simplemobiletools.calendar"
#APKPACKAGES[0]="com.simplemobiletools.calendar"
#APKPACKAGES[0]="com.simplemobiletools.calendar"
#APKPACKAGES[0]="com.simplemobiletools.calendar"

# AnExplorer - explore_files
APKPACKAGES[0]="dev.dworks.apps.anexplorer"
#APKPACKAGES[1]="dev.dworks.apps.anexplorer"
#APKPACKAGES[2]="dev.dworks.apps.anexplorer"
#APKPACKAGES[3]="dev.dworks.apps.anexplorer"

# Minetest - play_game
#APKPACKAGES[0]="net.minetest.minetest"
#APKPACKAGES[1]="net.minetest.minetest"
#APKPACKAGES[2]="net.minetest.minetest"
#APKPACKAGES[3]="net.minetest.minetest"

# Materialistic - read_article
#APKPACKAGES[0]="io.github.hidroh.materialistic"
#APKPACKAGES[1]="io.github.hidroh.materialistic"
#APKPACKAGES[0]="io.github.hidroh.materialistic"
#APKPACKAGES[0]="io.github.hidroh.materialistic"

#-------------------------------------------------------------------------------

LENGTHS=""
#LENGTHS="0sec" 
#LENGTHS=$LENGTHS" 1sec"
#LENGTHS="5sec"
LENGTHS="10sec"

# EBookDroid
#SLEEPS[0]=40
#SLEEPS[1]=44
#SLEEPS[2]=56
#SLEEPS[0]=71

#-------------------------------------------------------------------------------

echo ""
date '+%R %a %d %b %Y'
echo "Android Energy Experiment: Starting measurements..."
echo ""

begin=$(( $CORRECTION_VAL + 1 ))
trials=$(( $TRIAL_COUNT - $CORRECTION_VAL ))
# RUN ENERGY TESTS

for i in $REPLAYS
do
	# CUTS REPLAYS BY COMMA, SO INDIVIDUAL ITEMS CAN BE ACCESSED
	OLDIFS=$IFS
	IFS=','
	set $i
	IFS=$OLDIFS

	for ((i=0; i<${#VERSIONS[*]}; i++))
	do
		# RE/INSTALL APP
		$ADB install -r -d "$APKS/${APKNAMES[i]}"

		for LENGTH in $LENGTHS
		do
			for TRIAL_COUNT in $(seq $trials)
			do
				#CALCULATES TRIAL NUMBER
				TRIAL_COUNT=$(( $TRIAL_COUNT + $CORRECTION_VAL ))
				
				if [ "$TRIAL_COUNT" = "$begin" ]
				then
					echo ""
					echo "Testing $2 v. ${VERSIONS[i]} under scenario $3 at variation level $LENGTH ($trials trials)."	
				fi
				
				# RUN POWER MEASURING SCRIPT
				
				FILEPATH=${PROJ}/"data/raw_data/"${2}/${3}/${LENGTH}/${VERSIONS[i]}
				FILENAME=${2}-${3}-${LENGTH}-${VERSIONS[i]}
				FULLFILE=${FILEPATH}/${FILENAME}
				python -u $RUN_TRIAL -n $TRIAL_COUNT --android-sdk $ANDROID --reran $RERAN --length $LENGTH --file-name $FULLFILE $1
			done
		done
	done
done
echo "Testing complete."
echo ""
