from os import chdir, listdir, remove
from random import randrange

# INFO: This script will take a directory structure containing power traces and
# eliminate a user-defined (you) amount of sets of power traces (there are 4
# files generated during each trial, so each directory of 30 power traces (trials) has
# 120 files). The REASON this script was written to be able to COPY files from
# raw_data or energy_data into a seperate test directory. The purpose of this
# test directory is to be able to test scripts in development on some of the
# actual data they will eventually be used on without dealing with the various
# constraints of using the same enormous volume of data generated for the
# project. This also facilitates the mobility of the script-writing and testing
# process.
# INSTRUCTIONS:
# 1. BEFORE editing/running this script, copy a subsection of your collected
# data (either raw or at any stage of preprocessing) into your test directory.
# 2. This assumes your file/directory naming system matches ours as well. If
# it does not, you will have to change more then just the initial variables.
# 3. Set variables:
#      num_test_subjects - number of trails data you want left in each directory
#      total_num_trials - number of total trials in each directory
#      proj - string, path to project root directory
#      test_data_dir - the root of the TESTING power trace directory structure


# VARIABLES
num_test_subjects = 4
total_num_trials = 30

proj = "/home/green/Projects/AnEnergyEx"
test_data_dir = "/testing/data"


# MAIN
print('\nTrials for testing:')

chdir(proj + test_data_dir)

for dir in listdir():
    app = dir
    chdir(dir)
    print(' ',app)

    for sdir in listdir():
        scen = sdir
        chdir(sdir)
        print('   ',scen)

        for ssdir in listdir():
            var = ssdir
            chdir(ssdir)
            print('     ',var)

            for sssdir in listdir():
                ver = sssdir
                chdir(sssdir)

                test_subjects = []                
                i = num_test_subjects
                while i > 0:
                    subject = randrange(1+total_num_trials)
                    if subject not in test_subjects:
                        test_subjects.append(subject)
                        i = i-1

                print('       ',ver,'-',str(test_subjects))

                trace_files = listdir()
                for trace in trace_files:
                    if int(trace.replace(app,'').replace(scen,'').replace(var,'').replace(ver,'')
                              .replace('LOGCAT','').replace('RERAN','').replace('ttyACM0','')
                              .replace('ttyACM1','').replace('.csv','').replace('.log','')
                              .replace('-','')) not in test_subjects:

                        remove(trace)

                chdir('..')
            chdir('..')
        chdir('..')
    chdir('..')           

chdir(proj + test_data_dir)
