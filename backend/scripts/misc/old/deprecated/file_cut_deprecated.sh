# A power-trace contains data for a certain period of time, but sometimes
# the power-trace is too long. This script cuts a power-trace from the end
# to the given length. It looks at the first timestamp and finds the difference
# between the first timestamp and all consecutive ones. When the the difference
# is close to the given length, the script cuts the power-trace at that time stamp.

for f in *-ttyACM1.log
do
	head -n 7651 $f > $f.temp && rm $f && mv $f.temp $f
done
