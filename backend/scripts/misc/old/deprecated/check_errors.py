# 3 Jun 2018: I am currently using this script to test some data, so it is not going
# to function on regular data until I fix it.

from os import path, walk, chdir
import csv
import re
from natsort import natsorted, ns
import fnmatch
import itertools
from datetime import datetime

# NOTES: There are lots of debug messages. Just comment out the ones you're not currently using.

debuggingOn = True


def debugMsg(msg):
    if debuggingOn:
        print(msg)


# Sums the differences in the times between actions in the replay to find the
# total time of the replay
def findRecordingLength(reranlog):
    debugMsg(reranlog)
    with open(reranlog) as openedLog:
        for line in openedLog:
            if "Line Numbers" in line:
                break
        # Skip a line to get to the start of the log
        next(openedLog)
        recordingLength = 0
        for line in openedLog:
            timeValues = re.findall("(?<=[ ])[0-9]*(?=[ ])", line)
            seconds = long(timeValues[0])
            nanoseconds = float(timeValues[1])
            # timeDiff is in microseconds since powertrace files are in microseconds
            timeDiff = (seconds * 1000000) + (nanoseconds / 1000)
            debugMsg('Unmodified recording length: ' + str(recordingLength))
            debugMsg('Time "difference": ' + str(timeDiff))
            recordingLength = recordingLength + timeDiff
        return recordingLength


# Takes difference between first and last timestamps and checks if it is less
# than recordingLength
def checkPrematureExecution(trace, recordingLength):
    errors = [2]
    errors[0] = False
    #	debugMsg("      Initialize errors[] = " + str(errors))
    with open(trace) as openedTrace:
        traceReader = csv.reader(openedTrace)
        valueList = list(traceReader)
        firstTime = long(valueList[0][0])
        lastTime = long(valueList[len(valueList) - 1][0])
        timeDiff = lastTime - firstTime
        debugMsg("      First = " + str(firstTime) + " Last = " + str(lastTime) + " Diff = " + str(timeDiff))
        debugMsg("      Diff - recLen = " + str(timeDiff - recordingLength))
        #		if (timeDiff - recordingLength) < 0:
        #			debugMsg("*** Error condition SHOULD BE triggered")
        if timeDiff < recordingLength:
            # debugMsg("      Error condition triggered")
            errors[0] = True
            errors.append("PREMATURELY ENDING EXECUTION ERROR IN " + trace)
            debugMsg("      errors[] = " + str(errors))
    return errors


### BEGIN main ###

search_dir = '/Users/emily/AnEnergyEx/testing/data/AnkiDroid/make_deck/10sec/2.5_b'  # specific directory to search
outfile_contents = ''  # initialize string that will be written to output file aka outfile
outfile_contents = outfile_contents + str(datetime.today()) + '\n \n'
error_info = ''
errors = 0  # tracking total number of errors
debugMsg("Initialize error count: " + str(errors))

chdir(search_dir)

for x, y, files in walk('.'):
    print(files)
    # This is a sanity check
    #     if len(fileList) > 50:
    subject_info = search_dir.split("/")
    app = subject_info[5]
    scenario = subject_info[6]
    length = subject_info[7]
    version = subject_info[8]

    # Updating outfile and debugging
    debugMsg("\n Checking Errors for: " + " " +
             app + " " +
             scenario + " " +
             length + " " +
             version)

    time_now = str(datetime.today()).split()[1].split('.')[0]
    error_info = error_info + time_now + "  Checking Errors for: " + " " + app + " " + scenario + ' ' + length + " " + version

    ports = ["*ttyACM0*", "*ttyACM1*"]
    reranFileList = natsorted(fnmatch.filter(files, "*RERAN*"))

    for port in ports:
        dataFileList = natsorted(fnmatch.filter(files, port))
        for trace, reranlog in zip(dataFileList, reranFileList):
            # Updating outfile and debugging
            debugMsg("    Looking for premature executions in " + trace)
            # outfile_contents = outfile_contents + "    Looking for premature executions in " + trace + '\n'
            # Finding file names and recording lengths
            traceFilename = search_dir + "/" + trace
            reranlogFilename = search_dir + "/" + reranlog
            recordingLength = findRecordingLength(reranlogFilename)

            # debugMsg("      RERAN length: " + str(recordingLength/1000000) + " secs")

            # Checking for single error
            err = checkPrematureExecution(traceFilename, recordingLength)

            # debugMsg("      err[] = " + str(err))
            # debugMsg("      err[] SHOULD BE a duplicate of errors[]")

            # Updating outfile and error tracking
            if (err[0]):
                for error in err[1:]:
                    outfile_contents = outfile_contents + "    " + error + '\n'
                    errors = errors + 1
                    debugMsg(" Update error count: " + str(errors))

    outfile_contents = outfile_contents + '\n'

# Naming outfile
time = str(datetime.today()).replace(' ', '').replace(':', '').replace('.', '').replace('-', '')[4:-6]
outfile_name = "check_errors_" + time + ".txt"
# Creating, writing to, and closing outfile
outfile = open(outfile_name, "w")
outfile.write(outfile_contents)
outfile.close()
# Console output
if errors:
    print(str(errors) + " errors found. Refer to " + outfile_name + '.')
else:
    print("No errors found.")


### END main ###


### BEGIN unused ###

# UNUSED
# Subroutine to check timestamp consistency
def checkInconsistentTimestamps(dataFile):
    with open(dataFile, "rb") as f:
        reader = csv.reader(f)
        prevRow = 0;
        for row in reader:
            currRow = float(row[0])
            if (currRow <= prevRow):
                print("TIMESTAMP ERROR IN " + dataFile + "at row " + currRow, outfile)
            prevRow = currRow


# UNUSED
# Subroutine for checking for crashed executions
def checkCrashedExecution(logFile):
    # Construct regular expression object to match to
    matchExpression = re.compile("..ActivityManager.*Process.*has died and restarted.*")

    with open(logFile, "r") as f:
        for line in f:
            if (matchExpression.match(line) != None):
                print("CRASHED APP EXECUTION ERROR IN " + logFile)
