After gathering power traces and dumping into raw_data, do these preprocessing
steps to clean up the data so that it will be ready for analysis.

NOTE: There are specific, unique instructions for each stage of preprocessing.

TIPS: For each step that requires going through a bunch of subdirectories, 
consider googling how to do it via terminal before manually trudging through.

For admin use
########################
# Soft Paths: check_errors, log_to_csv, file_cut
# Ports: check_errors, file_cut
########################

1. check_errors.py

This script verifies that there were no mistakes made during data collection.
If there were mistakes made, the bad data will have to be re-collected (which
will take a good bit of time and set-up). Therefore, it's reccommended that
this script be run after each round of data collection (there is no issue with
running it on the same data twice). As long as there are no mistakes/things of
interest in the output files, the old ones can be deleted as soon as a new one
is generated (there will be no naming conflicts between output files).

Placement: copy to GTHA/raw_data/check_errors.py
Execute: from terminal, cd into raw_data, execute "python check_errors.py"
Output: console message and file check_errors_####.txt

2. Copy all contents of raw_data to energy_data. 

NOTE: Before doing this, go through all recursive subdirectories of raw_data 
and make sure there are no empty files. If there are any empty files (size 0 
bytes), re-collect the missing data.

3. log_to_csv.sh

This (very simple) script converts our '.log' power trace files to '.csv' files
in preparation for analysis. 

NOTE: After execution, go through all recursive subdirectories of energy_data 
and make sure there are no empty files. Sometimes the script will create 
empty files that are named something like "*tty*". Delete these (otherwise 
they will mess up analysis later).

Placement: copy to GTHA/energy_data/log_to_csv.sh
Execute: from terminal, cd into energy_data, execute "./log_to_csv.sh"
Output: .csv files

4. file_cut.py

This script checks for discrepencies in the length of a power trace and the
length of its corresponding recording.

NOTE: Although we now believe it to be fixed, our most recent bug was 
emptying some of our longer power trace files. This may be worth checking if
there are early errors in analysis.

Placement: GTHA/energy_data/[APP]/file_cut.py - copy to the top of each app 
directory in energy_data, ex. GTHA/energy_data/ConnectBot/file_cut.py
Execute: from terminal, for each app, cd into energy_data/[APP], execute 
"python3 file_cut.py"
Output: none

Now we move on to analysis!