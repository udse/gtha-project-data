#!/bin/bash

for d0 in */
i	cd $d0
	for d1 in */
	do
		cd $d1
		for d2 in */
		do
			cd $d2
			for d3 in */
			do
				cd $d3
				
				for file in *tty*.log
				do
					file_name=$(basename ${file} .log)
					echo -e "Time,Power\n$(cat ${file})" > ${file_name}.csv
					rm ${file}
				done

				cd ..
			done
			cd ..
		done
		cd ..
	done
	cd ..
done


