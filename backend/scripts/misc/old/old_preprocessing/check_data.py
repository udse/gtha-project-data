from os import chdir, listdir
from os.path import isdir
from math import sqrt
from datetime import datetime

# INFO: This script will take a directory structure containing power traces and
# check it for time inconsistancies between the RERAN log and each of the power
# traces for each trial. It will flag any significant (> 1 sec) time differences
# and generate and output file when finished. THIS SCRIPT MAY TAKE A WHILE TO RUN.
# By a while, I mean between 10 and 30 minutes depending on the volume of the data.
# INSTRUCTIONS:
# 1. Set variables:
#       num_trials - number of total trials in each directory
#       proj_dir - string, path to project root directory
#       data_dir - the root of the power trace directory structure
# 2. This assumes your file/directory naming system matches ours as well. If
# it does not, you will have to change more then just the initial variables.

# Root dir of project on specific machine
proj_dir = '/home/green/Projects/AnEnergyEx'
# Root dir of all data to be tested
data_dir = '/testing/data/old_data'
# Number of trials in each trial set
num_trials = 30


# Setting up easier debugging
debugging = True


# BEGIN debugging/developer functions

def dbg(x):
    if debugging:
        print(str(x))


# prints a formatted version of an element of data
def print_formatted(file_dict):
    app = ''
    scen = ''
    var = ''
    ver = ''
    mean_p0 = ''
    mean_p1 = ''
    sd_p0 = ''
    sd_p1 = ''
    odd = ''
    discrepancies = ''
    body_str = ''
    for x in file_dict:
        if x == 'app':
            app = file_dict[x]
        elif x == 'scen':
            scen = file_dict[x]
        elif x == 'var':
            var = file_dict[x]
        elif x == 'ver':
            ver = file_dict[x]
        elif x == 'mean_p0':
            mean_p0 = file_dict[x]
        elif x == 'mean_p1':
            mean_p1 = file_dict[x]
        elif x == 'sd_p0':
            sd_p0 = file_dict[x]
        elif x == 'sd_p1':
            sd_p1 = file_dict[x]
        elif x == 'flags':
            if file_dict[x] == []:
                discrepancies = 'No time discrepancies found.'
            else:
                discrepancies = discrepancies + str(len(file_dict[x])) + ' time discrepancies found.\n'
                for y in file_dict[x]:
                    discrepancies = discrepancies + '  There is a difference of ' + str(y[1]) + \
                                    ' secs between the RERAN log and the port ' + str(y[2]) + ' log in trial ' + \
                                    str(y[0]) + '.\n'
        elif int(x) in range(num_trials + 1):
            body_str = body_str + '  Trial ' + x + ':\n'
            body_str = body_str + '    RERAN length = ' + str(file_dict[x]['reran_len']) + '\n'
            body_str = body_str + '    Port 0 trace length = ' + str(file_dict[x]['0_len']) + ', mean = ' + \
                       str(file_dict[x]['0_mean']) + ', SD = ' + str(file_dict[x]['0_sd']) + '\n'
            body_str = body_str + '    Port 1 trace length = ' + str(file_dict[x]['1_len']) + ', mean = ' + \
                       str(file_dict[x]['1_mean']) + ', SD = ' + str(file_dict[x]['1_sd']) + '\n'
            body_str = body_str + '\n'
        else:
            odd = odd + str(x) + ', '

    odd = odd[:-2]

    body_str = body_str + discrepancies
    title_str = app + ' ' + scen + ' ' + var + ' ' + ver + '\n'
    title_str = title_str + ' Average power at port 0 = ' + str(mean_p0) + ', Average SD at port 0 = ' + \
                str(sd_p0) + '\n'
    title_str = title_str + ' Average power at port 1 = ' + str(mean_p1) + ', Average SD at port 1 = ' + \
                str(sd_p1) + '\n'
    print(title_str)
    print(body_str)
    print(odd)

# END debugging functions


# BEGIN file/calculation functions

# Creates set (dictionary) of 4 files corresponding to 1 trial
def make_file_sets(trial_set_path):
    files_by_trial = {}
    one_trial = {}
    files_by_trial['flags'] = []
    files_by_trial['mean_p0'] = 0.0
    files_by_trial['mean_p1'] = 0.0
    files_by_trial['sd_p0'] = 0.0
    files_by_trial['sd_p1'] = 0.0
    specific_execution = trial_set_path.split('/')
    files_by_trial['app'] = specific_execution[-4]
    files_by_trial['scen'] = specific_execution[-3]
    files_by_trial['var'] = specific_execution[-2]
    files_by_trial['ver'] = specific_execution[-1]
    for i in range(num_trials + 1):
        #dbg(listdir(trial_set_path))
        for file in listdir(trial_set_path):
            trial_num = int(file.replace(files_by_trial['app'], '').replace(files_by_trial['scen'], '')
                            .replace(files_by_trial['var'], '').replace(files_by_trial['ver'], '')
                            .replace('LOGCAT', '').replace('RERAN', '').replace('ttyACM0', '')
                            .replace('ttyACM1', '').replace('.csv', '').replace('.log', '').replace('-', ''))
            if trial_num == i:
                if file.find('RERAN') > 0:
                    one_trial['reran'] = file
                    #dbg(str(i) + ': RERAN')
                if file.find('LOGCAT') > 0:
                    one_trial['logcat'] = file
                    #dbg(str(i) + ': logcat')
                if file.find('ttyACM0') > 0:
                    one_trial['port0'] = file
                    #dbg(str(i) + ': port 0')
                if file.find('ttyACM1') > 0:
                    one_trial['port1'] = file
                    #dbg(str(i) + ': port 1')
                one_trial['reran_len'] = 0
                one_trial['0_mean'] = 0.0
                one_trial['1_mean'] = 0.0
                one_trial['0_sd'] = 0.0
                one_trial['1_sd'] = 0.0
                one_trial['0_pwrs'] = []
                one_trial['1_pwrs'] = []
                one_trial['0_t_scores'] = []
                one_trial['1_t_scores'] = []
                one_trial['0_len'] = 0
                one_trial['1_len'] = 0
        #dbg(one_trial)
        files_by_trial[str(i)] = one_trial
        one_trial = {}
    for i in range(num_trials + 1):
        if not files_by_trial[str(i)]:
            del files_by_trial[str(i)]

    return files_by_trial


def get_recording_len(reran_file):
    sec_sum = 0
    nano_sec_sum = 0
    skipped_lines = {}

    with open(reran_file) as work_file:
        for line in work_file:
            if line.find('Line Numbers') != -1:
                num_lines = int(line[line.find('=') + 1:].strip())
                for i in range(num_lines):
                    skipped_lines[i] = True
            if line.find('.') > 0:
                line_num = int(line[:line.find('.')].strip())
                skipped_lines[line_num-1] = False
                times = line.split(',')
                sec_sum = sec_sum + int(times[0][times[0].find('for') + 3:times[0].find('sec')].strip())
                nano_sec_sum = nano_sec_sum + int(times[1][:times[1].find('nsec')].strip())

    reran_sum = float(sec_sum) * 1000000 + float(nano_sec_sum) / 1000

    return reran_sum, skipped_lines


def get_trace_data(trace_file):
    pwr_sum = 0.0
    sd = 0.0
    pwr_list = {}
    t_list = {}
    time = []

    with open(trace_file) as work_file:
        i = 0
        temp_list = []
        #dbg(work_file)
        for line in work_file:
            temp_list = line.split(',')
            pwr = float(temp_list[1].replace('\n','').strip())
            pwr_list[i] = pwr
            pwr_sum = pwr_sum + pwr
            if i == 0:
                time.append(int(temp_list[0].strip()))
            i = i + 1
        if temp_list != []:
            time.append(int(temp_list[0].strip()))
        else:
            time = [0,0]

    tr_len = time[1] - time[0]
    #dbg(time)
    # calculating standard deviation
    if len(pwr_list) > 0:
        pwr_mean = pwr_sum / len(pwr_list)
    else:
        pwr_mean = 0

    temp_sum = 0.0
    for x in pwr_list:
        temp_sum = temp_sum + (pwr_list[x] - pwr_mean)**2

    if len(pwr_list) > 0:
        sd = sqrt(temp_sum / (len(pwr_list)-1))
    else:
        sd = 0

    # dbg('Mean: ' + str(pwr_mean))
    # dbg('#Measurements: ' + str(len(pwr_list)))
    # dbg('SD: ' + str(sd))

    # calculating t score
    # INSERT T-TEST HERE
    for x in pwr_list:
        if sd > 0:
            t_score = (pwr_list[x] - pwr_mean) / sd
        else:
            t_score = 0
        t_list[x] = t_score

    return tr_len, pwr_mean, sd, pwr_list, t_list


def compare_traces(file_dict):

    #print_formatted(file_dict)

    means0 = []
    sds0 = []
    means1 = []
    sds1 = []
    for x in file_dict:

        if type(file_dict[x]) != dict:
            pass
        elif int(x) in range(num_trials + 1):
            means0.append(file_dict[x]['0_mean'])
            means1.append(file_dict[x]['1_mean'])
            sds0.append(file_dict[x]['0_sd'])
            sds1.append(file_dict[x]['1_sd'])

    #dbg(means0)
    if len(means0) > 0:
        sm_mean0 = sum(means0) / len(means0)
        sm_sd0 = sum(sds0) / len(sds0)
    else:
        sm_mean0 = 0
        sm_sd0 = 0

    if len(means1) > 0:
        sm_mean1 = sum(means1) / len(means1)
        sm_sd1 = sum(sds1) / len(sds1)
    else:
        sm_mean1 = 0
        sm_sd1 = 0

    return sm_mean0, sm_sd0, sm_mean1, sm_sd1


def compare_times(file_dict):
    t_flags = []
    for x in file_dict:
        if type(file_dict[x]) != dict:
            pass
        elif int(x) in range(num_trials + 1):
            time_diff0 = abs(file_dict[x]['reran_len'] - file_dict[x]['0_len']) / 1000000
            time_diff1 = abs(file_dict[x]['reran_len'] - file_dict[x]['1_len']) / 1000000
            #dbg(x + ' ' + str(time_diff0))
            #dbg(x + ' ' + str(time_diff1))
            if time_diff0 > 1:
                t_flags.append([int(x), time_diff0, 0])
            if time_diff1 > 1:
                t_flags.append([int(x), time_diff1, 1])
    return t_flags

# Main function
def gather_data():

    print(str(datetime.today())[11:-4] + '  Starting data compilation.')

    data_list = []
    chdir(proj_dir + data_dir)

    for dir in listdir():
        if isdir(dir):
            app = dir
            chdir(dir)
            # dbg(' ' + app)

            for sdir in listdir():
                if isdir(sdir):
                    scen = sdir
                    chdir(sdir)
                    # dbg('   ' + scen)

                    for ssdir in listdir():
                        if isdir(ssdir):
                            var = ssdir
                            chdir(ssdir)
                            # dbg('     ' + var)

                            for sssdir in listdir():
                                if isdir(sssdir):
                                    ver = sssdir
                                    chdir(sssdir)

                                    file_sets = {}
                                    specific_ex = '/' + app + '/' + scen + '/' + var + '/' + ver
                                    full_path = proj_dir + data_dir + specific_ex

                                    file_sets = make_file_sets(full_path)

                                    for i in range(num_trials + 1):
                                        if str(i) in file_sets:
                                            for x in file_sets[str(i)]:
                                                if x == 'reran':
                                                    re_len, skips = get_recording_len(file_sets[str(i)][x])
                                                    file_sets[str(i)]['reran_len'] = re_len
                                                elif x == 'port0':
                                                    tr_len, tr_mean, tr_sd, tr_pwrs, tr_ts = get_trace_data(
                                                        file_sets[str(i)][x])
                                                    file_sets[str(i)]['0_len'] = tr_len
                                                    file_sets[str(i)]['0_mean'] = tr_mean
                                                    file_sets[str(i)]['0_sd'] = tr_sd
                                                    file_sets[str(i)]['0_pwrs'] = tr_pwrs
                                                    file_sets[str(i)]['0_t_scores'] = tr_ts
                                                elif x == 'port1':
                                                    tr_len, tr_mean, tr_sd, tr_pwrs, tr_ts = get_trace_data(
                                                        file_sets[str(i)][x])
                                                    file_sets[str(i)]['1_len'] = tr_len
                                                    file_sets[str(i)]['1_mean'] = tr_mean
                                                    file_sets[str(i)]['1_sd'] = tr_sd
                                                    file_sets[str(i)]['1_pwrs'] = tr_pwrs
                                                    file_sets[str(i)]['1_t_scores'] = tr_ts

                                    file_sets['mean_p0'], file_sets['sd_p0'], file_sets['mean_p1'], file_sets['sd_p1'] \
                                        = compare_traces(file_sets)
                                    file_sets['flags'] = compare_times(file_sets)

                                    data_list.append(file_sets)

                                    #print(str(datetime.today()) + '    Finished reading in one scenario.')

                                    chdir('..')
                            chdir('..')
                    chdir('..')
            print(str(datetime.today())[11:-4] + '    Finished reading in app: ' + app)

            # Fixing hardware issues
            generate_outfile(data_list)
            data_list = []

            chdir('..')

    chdir(proj_dir)

    return data_list

# END file functions


# BEGIN file generating functions

def format_entry(entry, chars_in_col):
    new_entry = entry
    for i in range(len(entry),chars_in_col):
        sep_str = new_entry.split(entry)
        if len(sep_str[0]) > len(sep_str[1]):
            new_entry = new_entry + ' '
        else:
            new_entry = ' ' + new_entry
    return new_entry


def generate_outfile(data_list):
    print(str(datetime.today())[11:-4] + '  Starting output generation.')

    all_flags = {}
    folder_list = ''
    odd = ''
    flag_count = 0

    time = str(datetime.today()).replace(' ', '').replace(':', '').replace('.', '').replace('-', '')[4:-6]
    outfile_name = "check_data_" + time + ".txt"

    outfile_contents = str(datetime.today()) + '    ' + proj_dir + data_dir + '\n \n'
    outfile_contents = outfile_contents + 'Checked ' + str(len(data_list)) + ' scenarios/folders for time discrepancies.\n'

    for folder in data_list:
        folder_list = folder_list + folder['app'] + ' ' + folder['scen'] + ' ' + folder['var'] + ' ' + \
                      folder['ver'] + '\n'
        if folder['flags'] != []:
            all_flags[data_list.index(folder)] = folder['flags']

    print(str(datetime.today())[11:-4] + '  Finished gathering fault info. Writing file.')

    if all_flags == {}:
        errors = 'No time discrepancies found.'
        flags = False
    else:
        flags = True
        scenarios = {}
        trials = {}
        details = {}

        for i in all_flags:
            #dbg(i)
            scenarios[i] = data_list[i]['app'] + ' ' + data_list[i]['scen'] + ' ' + data_list[i]['var'] + ' ' + data_list[i]['ver']
            trials[i] = []
            details[i] = {}
            details[i]['count'] = len(all_flags[i])
            for x in range(len(all_flags[i])):
                ind = all_flags[i][x][0]
                flag_count = flag_count + 1
                trials[i].append(ind)
                details[i]['re_len'] = data_list[i][str(ind)]['reran_len']
                if all_flags[i][x][2] == 0 or all_flags[i][x][2] == 1:
                    details[i]['td_0'] = all_flags[i][x][1]
                    details[i]['o_mean0'] = data_list[i]['mean_p0']
                    details[i]['o_sd0'] = data_list[i]['sd_p0']
                    details[i]['len_0'] = data_list[i][str(ind)]['0_len']
                    details[i]['mean_0'] = data_list[i][str(ind)]['0_mean']
                    details[i]['sd_0'] = data_list[i][str(ind)]['0_sd']
                    details[i]['td_1'] = all_flags[i][x][1]
                    details[i]['o_mean1'] = data_list[i]['mean_p1']
                    details[i]['o_sd1'] = data_list[i]['sd_p1']
                    details[i]['len_1'] = data_list[i][str(ind)]['1_len']
                    details[i]['mean_1'] = data_list[i][str(ind)]['1_mean']
                    details[i]['sd_1'] = data_list[i][str(ind)]['1_sd']
                else:
                    odd = odd + str(x)

    if flags:
        errors = str(flag_count) + ' time discrepancies found across ' + str(len(scenarios)) + ' scenarios.\n \n'
        for x in scenarios:

            form_scen = str(scenarios[x])
            form_scen = form_scen + ', in trial/s '

            for y in trials[x]:
                form_scen = form_scen + str(y) + ', '
            form_scen = form_scen[:-2] + '.'

            for j in range(len(form_scen),51):
                form_scen = form_scen + ' '

            #dbg(x)
            #dbg(details)
            #errors = errors + '  ' + form_scen + '    Mean (0) = ' + str(details[x]['o_mean0'])[:8] + ', SD (0) = ' + str(details[x]['o_sd0'])[:8] + '\n'
            #errors = errors + ' '*53 + '    Mean (1) = ' + str(details[x]['o_mean1'])[:8] + ', SD (1) = ' \
            #         + str(details[x]['o_sd1'])[:8] + '\n \n'


            errors = errors + '  ' + form_scen + '    Mean (0) = ' + str(details[x]['o_mean0'])[:8] + \
                         ', SD (0) = ' + str(details[x]['o_sd0'])[:8] + '\n'
            errors = errors + ' ' * 53 + '    Mean (1) = ' + str(details[x]['o_mean1'])[:8] + ', SD (1) = ' \
                         + str(details[x]['o_sd1'])[:8] + '\n \n'
            if details[x]['count'] > 1:
                errors = errors + '  ' + 'Both port 0 and port 1 traces have time discrepancies of more than one second' \
                                         ' (compared to the RERAN log).\n \n'
            elif details[x]['count'] == 1:
                     if details[x]['td_0'] > 1:
                         errors = errors + '  ' + 'The port 0 trace has a time difference of more than one second. \n \n'
                     elif details[x]['td_1'] > 1:
                         errors = errors + '  ' + 'The port 1 trace has a time difference of more than one second. \n \n'

            p0_diff_f = format_entry(str(details[x]['td_0'])[:8], 21)
            p1_diff_f = format_entry(str(details[x]['td_1'])[:8], 21)
            reran_f = details[x]['re_len'] / 1000000
            reran_f = format_entry(str(reran_f)[:8], 18)
            p0_len_f = details[x]['len_0'] / 1000000
            p0_len_f = format_entry(str(p0_len_f)[:8], 18)
            p1_len_f = details[x]['len_1'] / 1000000
            p1_len_f = format_entry(str(p1_len_f)[:8], 18)
            p0_mean_f = format_entry(str(details[x]['mean_0'])[:8], 10)
            p1_mean_f = format_entry(str(details[x]['mean_1'])[:8], 10)
            p0_sd_f = format_entry(str(details[x]['sd_0'])[:8], 10)
            p1_sd_f = format_entry(str(details[x]['sd_1'])[:8], 10)
            errors = errors + '  ' + '       | Time difference (s) | Length RERAN (s) | Length trace (s) ' \
                                         '|   Mean   |    SD    \n'
            errors = errors + '  ' + 'Port 0 |' + p0_diff_f + '|' + reran_f + '|' + p0_len_f + '|' + p0_mean_f + \
                         '|' + p0_sd_f + '\n'
            errors = errors + '  ' + 'Port 1 |' + p1_diff_f + '|' + reran_f + '|' + p1_len_f + '|' + p1_mean_f + \
                     '|' + p1_sd_f + '\n'
            errors = errors + '\n \n'

            # elif details[x]['count'] == 1:
            #     if details[x]['td_0'] > 1:
            #         errors = errors + '  ' + form_scen + '    Mean (0) = ' + str(details[x]['o_mean0'])[:8] + \
            #                  ', SD (0) = ' + str(details[x]['o_sd0'])[:8] + '\n'
            #         errors = errors + '  ' + 'The port 0 trace has a time difference of more than one second. \n \n'
            #         p0_diff_f = format_entry(str(details[x]['td_0'])[:8], 21)
            #         reran_f = details[x]['re_len'] / 1000000
            #         reran_f = format_entry(str(reran_f)[:8], 18)
            #         p0_len_f = details[x]['len_0'] / 1000000
            #         p0_len_f = format_entry(str(p0_len_f)[:8], 18)
            #         p0_mean_f = format_entry(str(details[x]['mean_0'])[:8], 10)
            #         p0_sd_f = format_entry(str(details[x]['sd_0'])[:8], 10)
            #
            #         errors = errors + '  ' + '       | Time difference (s) | Length RERAN (s) | Length trace (s) ' \
            #                                  '|   Mean   |    SD    \n'
            #         errors = errors + '  ' + 'Port 0 |' + p0_diff_f + '|' + reran_f + '|' + p0_len_f + '|' + p0_mean_f + \
            #                  '|' + p0_sd_f + '\n'
            #
            #     elif details[x]['td_1'] > 1:
            #         errors = errors + '  ' + form_scen + '    Mean (1) = ' + str(details[x]['o_mean1'])[:8] + ', SD (1) = ' \
            #              + str(details[x]['o_sd1'])[:8] + '\n \n'
            #         errors = errors + '  ' + 'The port 1 trace has a time difference of more than one second. \n \n'
            #         p1_diff_f = format_entry(str(details[x]['td_1'])[:8], 21)
            #         reran_f = details[x]['re_len'] / 1000000
            #         reran_f = format_entry(str(reran_f)[:8], 18)
            #         p1_len_f = details[x]['len_1'] / 1000000
            #         p1_len_f = format_entry(str(p1_len_f)[:8], 18)
            #         p1_mean_f = format_entry(str(details[x]['mean_1'])[:8], 10)
            #         p1_sd_f = format_entry(str(details[x]['sd_1'])[:8], 10)
            #
            #         errors = errors + '  ' + '       | Time difference (s) | Length RERAN (s) | Length trace (s) ' \
            #                                  '|   Mean   |    SD    \n'
            #         errors = errors + '  ' + 'Port 1 |' + p1_diff_f + '|' + reran_f + '|' + p1_len_f + '|' + p1_mean_f + \
            #                  '|' + p1_sd_f + '\n'

            errors = errors + '\n \n'



    outfile_contents = outfile_contents + errors + '\n'
    outfile_contents = outfile_contents + 'Scenarios evaluated: \n'
    outfile_contents = outfile_contents + folder_list + '\n \n'
    if odd != '':
        outfile_contents = outfile_contents + 'Weird things: \n' + odd + '\n'

    outfile = open(outfile_name, "w")
    outfile.write(outfile_contents)
    outfile.close()

    if flags:
        print(str(datetime.today())[11:-4] + '  Time inconsistencies found. Please refer to ' + outfile_name + '.')
    else:
        print(str(datetime.today())[11:-4] + '  No time inconsistencies found. Summary output in ' + outfile_name + '.')

# END file generating functions

# BEGIN main

data = gather_data()
#generate_outfile(data)

# END main
