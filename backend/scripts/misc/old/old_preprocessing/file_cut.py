#!/usr/bin/env python3

# A power-trace contains data for a certain period of time, but due to measuring
# difficulties, power-traces are typically longer than the time specified by the
# replay. Sometimes this is a few microseconds, sometimes it is many seconds.
# Regardless this should be accounted for.
#
# This script cuts the power-trace to the length of the replay. It does this by 
# looking at the RERAN log to determine the actual length. We look at the log
# rather than the replay file itself since of variations that occur during
# recording. We want the power-trace to reflect the actual recorded length rather
# than the desired.
#
# Note: power-trace .csv files are assumed to have headers
#
# To use this script: 
# 1. Place it in the directory with the power-traces
# 2. Specify the inputs
# 3. Execute the script

import os
from os.path import join
import fnmatch
import csv
from natsort import natsorted
import re

# Sums the differences in the times between actions in the replay to find the
# total time of the repaly
def findRecordingLength(reranlog):
	with open(reranlog) as f:
		for line in f:
			if "Line Numbers" in line:
				break
		# Skip a line to get to the start of the log
		next(f)
		recordingLength = 0
		for line in f:
			timeValues = re.findall("(?<=[ ])[0-9]*(?=[ ])", line)
			seconds = int(timeValues[0])
			nanoseconds = int(timeValues[1])
			# timeDiff is in microseconds since powertrace files are in microseconds
			timeDiff = (seconds * 1000000) + (nanoseconds / 1000)
			recordingLength = recordingLength + timeDiff
		return recordingLength

# Finds the proper line to cut at
def findLineToCutAt(trace, recordingLength):
	with open(trace) as f:
		traceReader = csv.reader(f)

		# Move reader to next row to skip header
		next(traceReader)

		# read start timestamp		
		start = int(next(traceReader)[0])
		
		cutLine = 2
		# Enumerate to keep track of row information as well as their indices.
		for row in traceReader:
			cutLine = cutLine + 1
			current = int(row[0])			
			if (current - start) >= recordingLength:
				break
	return cutLine

def truncate(file, lineToCutAt):
	lines = open(file).readlines()
	open(file, "w").writelines(lines[:lineToCutAt])

# BEGIN MAIN CODE

# Do this for all power-trace for all subjects
for root, dirs, files in os.walk("."):
	traceFiles = [join(root, f) for f in natsorted(fnmatch.filter(files, "*ttyACM0*"))]
	reranFiles = [join(root, f) for f in natsorted(fnmatch.filter(files, "*RERAN*"))]
		
	assert len(traceFiles) == len(reranFiles)

	for trace, reranlog in zip(traceFiles, reranFiles):
		recordingLength = findRecordingLength(reranlog)
		lineToCutAt = findLineToCutAt(trace, recordingLength)

		# Open the file again and cut the power-trace at the correct line
		truncate(trace, lineToCutAt)