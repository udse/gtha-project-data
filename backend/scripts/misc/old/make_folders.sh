#!/bin/bash

# Change these as required
curr_dir=$(pwd)
energy_dir=$curr_dir/energy_data
raw_dir=$curr_dir/raw_data
replay_dir=$curr_dir/replays
diffs_dir=$curr_dir/analysis/diffs
plots_dir=$curr_dir/analysis/plots
summary_dir=$curr_dir/analysis/summary
energies_dir=$curr_dir/analysis/energies

# Change as needed
#app="Financius"
#scenario="write_transactions"
#versions="0.12.1 0.13.0 0.14.0 0.15.0"

#app="EBookDroid"
#scenario="open_pdf"
#versions="1.1.0_b_m 1.1.2_b_m 1.1.4_f_m 1.2.0_f"

#app="Shortyz"
#scenario="download_puzzles"
#versions="4.0.0_b 4.0.2_b_m 4.0.5_b_m 4.0.7_f"

app="AnkiDroid"
scenario="learn_cards"
versions="2.5_b 2.6_b 2.7_f 2.8_f"

#app="AnkiDroid"
#scenario="make_deck"
#versions="2.5_b 2.6_b 2.7_f 2.8_f"

#app="K9Mail"
#scenario="polling"
#versions="1.993_b 2.000_b 2.391_f 2.505_f"

#app="ConnectBot"
#scenario="connect_ssh"
#versions="1.8.3_b 1.8.6_b 1.8.6_f_m 1.9.0a2_f"

#app="ConnectBot"
#scenario="ssh_shell"
#versions="1.8.3 1.8.4 1.8.5 1.8.6"

#app="SimpleCalendar"
#scenario="make_events"
#versions="8003feb 5379244 b2fe795 fd2ec79"

#app="AnExplorer"
#scenario="explore_files"
#versions="e7e51f8 419112b 53086f3 d9a7443"

#app="Minetest"
#scenario="play_game"
#versions="0.4.12 0.4.13 0.4.14 0.4.15"

#app="Materialistic"
#scenario="read_article"
#versions="57 58 59 60"

#app="TestApp1"
#scenario="test_scenario_1"
#versions="1.0_b 1.1_f 1.2_f 1.3_b"

#app="TestApp2"
#scenario="test_scenario_2"
#versions="2.0_b 2.1_f 2.2_f 2.3_b"

lengths="0sec 1sec 5sec 10sec"
measures="EuclideanDistance FourierDistance EDRDistance DTWDistance MinJumpCost ARPicDistance TWED"

# Make directories for replays.
mkdir -p ${replay_dir}/${app}/${scenario}

# Make directories for energy_data and results
for version in $versions
do
	for length in $lengths
	do
		for measure in $measures
		do
			mkdir -p ${energy_dir}/${app}/${scenario}/${length}/${version}
			mkdir -p ${raw_dir}/${app}/${scenario}/${length}/${version}
			mkdir -p ${diffs_dir}/${app}/${scenario}/${length}/${measure}
			mkdir -p ${plots_dir}/${app}/${scenario}/${length}
			mkdir -p ${summary_dir}/${app}/${scenario}/${length}/${measure}
			mkdir -p ${energies_dir}/${app}/${scenario}/${length}
		done
	done
done
