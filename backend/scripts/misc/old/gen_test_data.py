from random import randint

# App name
#app = "TestApp1"
app = "TestApp2"

# Scenario name
#scenario = "test_scenario_1"
scenario = "test_scenario_2"

# Length name
length = ["0sec", "1sec", "5sec", "10sec"]

# Version name
#version = ["1.0_b", "1.1_f", "1.2_f", "1.3_b"]
version = ["2.0_b", "2.1_f", "2.2_f", "2.3_b"]

# The number of test power-traces to generate
trial_num = 30

# Number of measurements for the trace
mes_num = [50, 100, 500, 1000]

# Range of numbers power values are chosen from: [min_range, max_range]
#ranges = [[100, 110], [10, 20], [10, 20], [200, 210]]
ranges = [[500, 510], [100, 110], [100, 110], [500, 510]]

for length_index,chosen_length in enumerate(length):
	chosen_mes_num = mes_num[length_index]

	for version_index,chosen_version in enumerate(version):
		chosen_min = ranges[version_index][0]
		chosen_max = ranges[version_index][1]

		for i in range(1, trial_num + 1):
			filepath = "./" + app + "/" + scenario + "/" + chosen_length + "/" + chosen_version + "/"
			filename = app + "-" + scenario + "-" + chosen_length + "-" + chosen_version + "-" + str(i) + "-ttyACM1.log"
			write_file = open(filepath + filename, mode='w')

			for j in range(1, chosen_mes_num + 1):
				rand_num = randint(chosen_min, chosen_max)
				write_file.write(str(j) + "," + str(rand_num) + "\n")
