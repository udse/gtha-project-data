from os import listdir, mkdir
from os.path import isdir

proj = '/home/emily/AnEnergyEx/'
scenarios = proj + 'set-up/reference/input.csv'
new_root = proj + 'analysis/classification/'

d1 = ['0','1','5','10']
d2 = ['Euclidean','Fourier','Dynamic Time Warping','Edit Distance on Real Sequences','Minimum Jump Cost Dissimilarity',
      'Auto-Regressive Model','Time-Warped Edit Distance']



def make_dir_struct(root, scens, depth):
    scen_list = get_scenarios(scens)

    for scen in scen_list:
        scen_dir = root + '/' + scen
        if scen not in listdir(root):
            mkdir(scen_dir)

        if depth > 1:
            for t in d1:
                d1_dir = scen_dir + '/' + t
                if t not in listdir(scen_dir):
                    mkdir(d1_dir)

                if depth > 2:
                    for s in d2:
                        d2_dir = d1_dir + '/' + s
                        if s not in listdir(d1_dir):
                            mkdir(d2_dir)


def get_scenarios(scens):
    scen_list = []
    scen_file = open(scens,'r')
    for scen in scen_file:
        if '#' not in scen:
            scen_list.append(scen.strip())
    return scen_list


make_dir_struct(new_root,scenarios,2)
