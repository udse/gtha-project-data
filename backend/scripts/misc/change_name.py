from os import chdir, listdir, rename
from os.path import isdir

proj_dir = '/home/emily/AnEnergyEx'
root_dir = '/data/metadata'


leading = ['-']


def strip_leading(base):
    for d in listdir(base):
        if isdir(base + '/' + d):
            strip_leading(base + '/' + d)
        else:
            for x in leading:
                if d[0] == x:
                    rename((base + '/' + d), (base + '/' + d[1:]))


def change_file_name(base,orig,new):
    for d in listdir(base):
        if isdir(base + '/' + d):
            change_file_name(base + '/' + d,orig,new)
        else:
            for x in orig:
                if x in d:
                    rename((base + '/' + d), (base + '/' + d.replace(x, new[orig.index(x)])))


def change_dir_name(base,orig,new):
    #print(orig)
    #print(new)
    for d in listdir(base):
        #print(d)
        if isdir(base + '/' + d):
            for x in orig:
                if x in (base + '/' + d):
                    rename((base + '/' + d),(base + '/' + d).replace(x,new[orig.index(x)]))
    for sdir in listdir(base):
        if isdir(base + '/' + sdir):
            change_dir_name(base + '/' + sdir,orig,new)


def change_ver_name(base,scen_dict):
    for scen in scen_dict:
        dirs = listdir(base)
        for d in dirs:
            if d == scen and isdir(base + '/' + d):
                orig = scen_dict[scen][0]
                new = scen_dict[scen][1]
                change_dir_name(base,orig,new)
                change_file_name(base,orig,new)
                orig = []
                new = []


#apps = ['ConnectBot','AnExplorer','AnkiDroid','Materialistic','Minetest','Financius','SimpleCalendar','EBookDroid','Shortyz']

#for app in apps:
#    orig.append(app)
#    new.append('')

# with open('abbreviations.csv','r') as abb:
#     for line in abb:
#         temp = line.strip().split(',')
#         orig.append(temp[0])
#         new.append(temp[1])

scens = {}
orig = []
new = []


with open('versions.csv','r') as ver:
    for line in ver:
        temp = line.strip('\n').split(',')
        orig.append(temp[1])
        orig.append(temp[2])
        orig.append(temp[3])
        orig.append(temp[4])
        new.append('v1')
        new.append('v2')
        new.append('v3')
        new.append('v4')
        scens[temp[0]] = [orig,new]
        orig = []
        new = []


change_ver_name(proj_dir + root_dir,scens)

#change_dir_name(proj_dir + root_dir)
#change_file_name(proj_dir + root_dir)
#strip_leading(proj_dir + root_dir)