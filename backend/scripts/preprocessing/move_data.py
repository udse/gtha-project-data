from os import chdir, listdir, mkdir, rename
from os.path import isdir
from shutil import copyfile

proj = '/home/emily/AnEnergyEx/'
data = 'data/'
raw = data + 'raw_data/'
ready = data + 'ready_data/'
ports = data + 'metadata/'


def pick_files(port_dict,raw_base,ready_base):
    for scen in port_dict:
        scen_list = listdir(raw_base)
        if scen in scen_list:
            for var in port_dict[scen]:
                var_list = listdir(raw_base + '/' + scen)
                if var in var_list:
                    for ver in port_dict[scen][var]:
                        ver_list = listdir(raw_base + '/' + scen + '/' + var)
                        if ver in ver_list:
                            files = listdir(raw_base + '/' + scen + '/' + var + '/' + ver)
                            for i in range(len(port_dict[scen][var][ver])):
                                match = '-' + str(i+1) + '-ttyACM' + str(port_dict[scen][var][ver][i])
                                for file in files:
                                    if match in file:
                                        copy_to_ready(raw_base + '/' + scen + '/' + var + '/' + ver + '/' + file, ready_base)


def copy_to_ready(trace_addr,ready_base):
    folders = trace_addr.split('/')
    ready_scens = listdir(ready_base)
    if folders[-4] not in ready_scens:
        mkdir(ready_base + '/' + folders[-4])
    ready_vars = listdir(ready_base + '/' + folders[-4])
    if folders[-3] not in ready_vars:
        mkdir(ready_base + '/' + folders[-4] + '/' + folders[-3])
    ready_vers = listdir(ready_base + '/' + folders[-4] + '/' + folders[-3])
    if folders[-2] not in ready_vers:
        mkdir(ready_base + '/' + folders[-4] + '/' + folders[-3] + '/' + folders[-2])
    new_addr = copyfile(trace_addr,ready_base + '/' + folders[-4] + '/' + folders[-3] + '/' + folders[-2] + '/' + folders[-1])
    rename(new_addr,new_addr.replace('.log','.csv'))


def get_ports(port_base):
    port_dict = {}
    for d in listdir(port_base):
        scen_dir = port_base + '/' + d
        if isdir(scen_dir):
            for f in listdir(scen_dir):
                if 'ports' in f:
                    port_dict[d] = read_ports(scen_dir + '/' + f)
    return port_dict


def read_ports(file_addr):
    scen_dict = {}
    first = True
    for line in open(file_addr,'r'):
        if first:
            first = False
            pass
        else:
            port_line = line.split(',')
            if port_line[0] not in scen_dict:
                scen_dict[port_line[0]] = {}
            if port_line[1] not in scen_dict[port_line[0]]:
                scen_dict[port_line[0]][port_line[1]] = []
            scen_dict[port_line[0]][port_line[1]] = scen_dict[port_line[0]][port_line[1]][:int(port_line[2])-1] + [int(port_line[3])] + scen_dict[port_line[0]][port_line[1]][int(port_line[2])-1:]
    return scen_dict


# Main
print('Copying data from ' + proj + raw + ' to ' + proj + ready + '. This make take a moment.')
ports_by_trial = get_ports(proj + ports)
pick_files(ports_by_trial,proj + raw,proj + ready)
print('Finished.')
