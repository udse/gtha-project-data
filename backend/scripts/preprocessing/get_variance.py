from os import chdir, listdir, mkdir
from os.path import isdir
#from math import sqrt
from datetime import datetime

# Root dir of project on specific machine
proj_dir = '/home/emily/AnEnergyEx'
# Root dir of all data to be tested
data_dir = '/data/raw_data'
# Number of trials in each trial set
num_trials = 30


debugging = True

def dbg(x):
    if debugging:
        print(str(x))



def find_files(base, done):
    if len(base.split('/')) == 7:
        print(str(datetime.today())[11:-7] + '  ' + str(base.split('/')[6]))
    for d in listdir(base):
        if isdir(base + '/' + d):
            done.append(base)
            find_files(base + '/' + d,done)
    if base not in done:
        chdir(base)
        results(base)


def gather_data(base):
    pwr_file_list = []
    en_list = []
    file_list = listdir(base)

    i = 0
    while (i < (num_trials + 1)):
        hit = False
        sum0 = 0.0
        sum1 = 0.0
        p0 = ''
        p1 = ''

        for doc in file_list:
            if (str('-' + str(i) + '-' + 'ttyACM0') in doc):
                p0 = doc
                sum0 = get_trace_sum(doc)
                hit = True
            elif (str('-' + str(i) + '-' + 'ttyACM1') in doc):
                p1 = doc
                sum1 = get_trace_sum(doc)
                hit = True
            else:
                pass

        if sum0 > sum1 and hit:
            en_list.append(sum0)
            pwr_file_list.append(p0)

        elif sum1 > sum0 and hit:
            en_list.append(sum1)
            pwr_file_list.append(p1)

        i = i + 1

    return pwr_file_list, en_list


def calc_variance(e_list):
    if (len(e_list) > 0):
        dev_list = []
        en_mean = sum(e_list) / len(e_list)

        for en in e_list:
            dev_list.append((en-en_mean)**2)

        variance = sum(dev_list) / len(dev_list)

        return variance
    else:
        return 0.0


def get_trace_sum(trace_file):
    with open(trace_file) as work_file:
        i = 0
        en_sum = 0.0
        end_time = 1.0
        temp_list = []
        for line in work_file:
            temp_list = line.split(',')
            pwr = float(temp_list[1].replace('\n','').strip())*(10**(-3))
            # pwr measured in milliwatts
            # time measured in micro secs
            st_time = end_time
            end_time = float(temp_list[0].strip())*(10**(-6))

            #converting to energy
            if i != 0:
                en_sum = en_sum + (end_time - st_time) * pwr
            else:
                en_sum = en_sum + pwr
            i = i + 1

    return en_sum


def results(base):
    pwr_files, energies = gather_data(base)
    temp = base.split('/')

    scen = temp[-3]
    var = temp[-2]
    ver = temp[-1]

    outfile_name = var + '-' + ver + '-energy.csv'
    outfile_contents = 'Trial,Energy (J)\n'
    port_file_name = scen + '-ports.csv'
    port_file_contents = ''
    
    if len(pwr_files) == num_trials and len(energies) == num_trials:
        for i in range(num_trials):
            details = pwr_files[i].split('-')
            outfile_contents = outfile_contents + details[3] + ',' + str(energies[i]) + '\n'
            port_file_contents = port_file_contents + details[1] + ',' + details[2] + ',' + details[3] + ',' + pwr_files[i][-5] + '\n'
    elif len(pwr_files) != len(energies):
        dbg('Discrepancy: ' + str(len(pwr_files)) + ' files, ' + str(len(energies)) + ' sums')
        dbg(energies)
        dbg(pwr_files)

    chdir(proj_dir + '/data/metadata')

    v_file = open('variance.csv', 'a')
    v_file.write(scen + ',' + var + ',' + ver + ',' + str(calc_variance(energies)) + '\n')
    v_file.close()

    #print(scen)
    if scen not in listdir():
        mkdir(scen)
    
    chdir(scen)

    if port_file_name not in listdir():
        port_file = open(port_file_name, 'a')
        port_file.write('Var (s),Version,Trial,Port\n')
        port_file.close()

    port_file = open(port_file_name, 'a')
    port_file.write(port_file_contents)
    port_file.close()
    
    outfile = open(outfile_name, 'w')
    outfile.write(outfile_contents)
    outfile.close()


#main
chdir(proj_dir + '/data/metadata')
file = open('variance.csv', 'w')
file.write('Scen,Var (s),Version,Variance\n')
file.close()
chdir(proj_dir + data_dir)

print()
print(str(datetime.today())[:-7] + '  Testing variance in ' + proj_dir + data_dir)
print()

find_files(proj_dir + data_dir, [str(proj_dir + data_dir)])

print()
print(str(datetime.today())[:-7] + '  Testing complete')
