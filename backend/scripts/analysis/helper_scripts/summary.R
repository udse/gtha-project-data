# This file provides the functions needed to perform the hypothesis testing step and
# returns the output of the system. Rather than outputting the result of a single
# versions comparison, it outputs the result of all of them in a single table.

# Source necessary files
proj.dir <- "/home/emily/AnEnergyEx"
prep.addr <- paste(proj.dir,"set-up/reference", sep = "/")
helper.scripts.dir <- paste(proj.dir,"backend/scripts/analysis/helper_scripts", sep = "/")
source(paste(helper.scripts.dir, "CommonLanguageEffectSizes.R", sep = "/"))

# INPUT: A list of data frames with distance data for calculating the threshold
#        A list of data frames with distance data for scoring
#        A list of ordered pairs with version information
# OUTPUT: A data frame that summarized the given data
Summarize <- function(within.diffs.list, between.diffs.list, energies, version.comparisons) {
    summary <- data.frame(Before.Version = character(0),
                          After.Version = character(0),
                          Diff = logical(0),
                          Direction = character(0),
                          P.Value = numeric(0),
                          Simple.Diff = numeric(0),
                          Within.Diffs.Mean = numeric(0),
                          Within.Diffs.SD = numeric(0),
                          Between.Diffs.Mean = numeric(0),
                          Between.Diffs.SD = numeric(0),
                          Runtime = numeric(0),
                          stringsAsFactors = FALSE)

    for (i in 1:length(version.comparisons)) {
        summary <- rbind(summary,
                         data.frame(Before.Version = version.comparisons[[i]][1],
                                    After.Version = version.comparisons[[i]][2],
                                    Diff = FALSE,
                                    Direction = "",
                                    P.Value = NA,
                                    Simple.Diff = NA,
                                    Within.Diffs.Mean = NA,
                                    Within.Diffs.SD = NA,
                                    Between.Diffs.Mean = NA,
                                    Between.Diffs.SD = NA,
                                    Runtime = NA,
                                    stringsAsFactors = FALSE))
    }

    for (i in 1:length(version.comparisons)) {
        within.diffs <- within.diffs.list[[i]]
        between.diffs <- between.diffs.list[[i]]

        within.diffs.values <- within.diffs[1:nrow(within.diffs), 3]
        within.diffs.mean <- mean(within.diffs.values)
        within.diffs.sd <- sd(within.diffs.values)

        between.diffs.values <- between.diffs[1:nrow(between.diffs), 3]
        between.diffs.mean <- mean(between.diffs.values)
        between.diffs.sd <- sd(between.diffs.values)

        test.results <- try(wilcox.test(between.diffs.values, 
                                       within.diffs.values, 
                                       conf.level = .95, 
                                       paired = FALSE, 
                                       alternative = "greater"), 
                                       silent=TRUE)
        
        if (class(test.results) == "try-error") {
            next
        }

        u.statistic <- test.results[1]
        param <- test.results[2]
        p.value <- test.results[3]

        df.for.cles <- data.frame(Distance = c(within.diffs.values, between.diffs.values), Database = c(rep("Within", length(within.diffs.values)), rep("Between", length(between.diffs.values))))
        prop.fav <- cles_brute.fnc(variable = "Distance", group = "Database", baseline = "Between", data = df.for.cles)
        prop.unfav <- 1 - prop.fav
        simple.difference <- prop.fav - prop.unfav

        alpha <- .05
        effect.size.threshold <- .8
        is.diff <- FALSE
        if (p.value <= alpha && simple.difference > effect.size.threshold) {
            is.diff <- TRUE
        }

        #print(version.comparisons[i])
        before.ver.num <- substr(version.comparisons[i][[1]][1],2,2)
        after.ver.num <- substr(version.comparisons[i][[1]][2],2,2)
        before.energy <- energies[[before.ver.num,1]]
        after.energy <- energies[[after.ver.num,1]]
        direction <- NA
        if (is.diff) {
            if (after.energy - before.energy > 0) {
                direction <- "POSITIVE"
            } else if (after.energy - before.energy < 0) {
                direction <- "NEGATIVE"
            }
        }


        within.diffs.runtimes <- within.diffs[1:nrow(within.diffs), 4]
        between.diffs.runtimes <- between.diffs[1:nrow(between.diffs), 4]

        within.diffs_sum.runtimes <- sum(within.diffs.runtimes)
        between.diffs_sum.runtimes <- sum(between.diffs.runtimes)
        sum.runtimes <- sum(c(within.diffs_sum.runtimes, between.diffs_sum.runtimes))

        # Add information into a table
        summary[[i, 3]] <- is.diff
        summary[[i, 4]] <- direction
        summary[[i, 5]] <- round(p.value[[1]], digits <- 3)
        summary[[i, 6]] <- round(simple.difference, digits = 3)
        summary[[i, 7]] <- round(within.diffs.mean, digits = 3)
        summary[[i, 8]] <- round(within.diffs.sd, digits = 3)
        summary[[i, 9]] <- round(between.diffs.mean, digits = 3)
        summary[[i, 10]] <- round(between.diffs.sd, digits = 3)
        summary[[i, 11]] <- round(sum.runtimes, digits = 3)
    }

    return(summary)
}

# INPUT: A value for the sample mean
#                A value for the expected mean
#                A value for the s.d. of the sample
# OUTPUT: A value that says how many standard deviations away the
#               sample mean and expected mean are from each other.
CohensD <- function(mean, mu, sample.sd, expected.sd) {
    pooled.sd <- sqrt((sample.sd^2 + expected.sd^2) / 2)
    d <- (mean - mu) / pooled.sd
    return(d)
}
