from numpy import *

def dissim(x,y,beta):
	return min(getdxy(x,y,beta),getdxy(y,x,beta))

def getdxy(x,y,beta):
	M=len(x)
	N=len(y)
	sigma=std(y)
	phi=beta*4.0*sigma/float(min(M,N))
	tx=0
	ty=0
	dxy=0
	while tx<M and ty<N:
		cmin,tx,ty=getcmin(x,tx,y,ty,phi)
		dxy+=cmin
		if tx>=M or ty>=N: break
		cmin,tx,ty=getcmin(y,ty,x,tx,phi)
		dxy+=cmin
	return dxy

def getcmin(x,tx,y,ty,phi):
	M=len(x)
	cmin=10**10
	delta=0
	deltamin=0
	while ty+delta<M:
		c=(phi*delta)**2
		if c>cmin:
			if ty+delta>tx: break
			continue
		c+=(x[tx]-y[ty+delta])**2
		if c<cmin:
			cmin=c
			deltamin=delta
		delta+=1
	return cmin,tx+1,ty+deltamin+1

