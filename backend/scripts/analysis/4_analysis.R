# This file provides driver code to analyze the results of the experiment.

# Load libraries

# Source necessary files
proj.dir <- "/home/emily/AnEnergyEx"
prep.addr <- paste(proj.dir,"set-up/reference", sep = "/")
helper.scripts.dir <- paste(proj.dir,"backend/scripts/analysis/helper_scripts", sep = "/")
source(paste(helper.scripts.dir, "bin_class_analysis.R", sep = "/"))

# Initialize variables
var.levels <- c("0", "1", "5", "10")
num.measures <- 7
input_scens <- read.csv(paste(prep.addr, "input.csv", sep = "/"), header = FALSE, comment.char = "#")
input_scens <- data.frame(lapply(input_scens, as.character), stringsAsFactors = FALSE)
abbr <- read.csv(paste(prep.addr, "abbreviations.csv", sep = "/"), header = FALSE)
abbr <- data.frame(lapply(abbr, as.character), stringsAsFactors = FALSE)

# Finding names and abbreviations of distance measures
diff.names <- character(num.measures)
diff.abbr <- character(num.measures)
for (k in 1:num.measures) {
    diff.names[k] <- abbr[k+20,1]
    diff.abbr[k] <- abbr[k+20,2]
    #print(paste(k,diff.names[k],diff.abbr[k],sep = ', '))
}

# Begin main code
print("Running analysis for the experiment.")

output <- data.frame(Measure = "",
                     Sec_0 = "",
                     Sec_1 = "",
                     Sec_5 = "",
                     Sec_10 = "",
                     stringsAsFactors = FALSE)

for (i in 1:length(diff.names)) {
		output[i,1] = diff.names[i]
    for (j in 1:length(var.levels)) {
        cell.value <- BinClassify(diff.abbr[i], var.levels[[j]])
        output[i,j+1] <- paste(unlist(cell.value), collapse=',')
    }
}

# Write information into a table
out.file.path <- paste(proj.dir, "analysis", "experiment_analysis.csv", sep = "/")
write(x = "Cell Format - (TP, FP, TN, FN, Sensitivity, Specificity)", file = out.file.path, ncolumns = 1)
write(x = "\n", file = out.file.path, ncolumns = 1, append = TRUE)               
write.table(output, out.file.path, row.names = FALSE, append = TRUE, sep = ",", dec = ".")

