APPLICATION CLASSES:
1. Apps with an energy bug and then a fix but not necessarily 
   many feature changes.
2. Apps with feature changes between versions
3. Apps where versions are from consecutive commits.

APPLICATIONS CONSIDERED:
Shortyz:
	http://www.kebernet.net/Home/projects/shortyz
	Provides downloads for .apk files for various versions

	https://github.com/kebernet/shortyz
	Github repository. Unfortunately, first commit is the source code
	for the last version of Shortyz we considered. We're not sure
	where source code for other versions are.

	https://code.google.com/archive/p/shortyz/
	Google Code archive. We're not sure if it has source code for
	other versions.

ConnectBot:
	https://github.com/connectbot/connectbot
	Github repository. Contains all versions we used.

EBookDroid:
	https://code.google.com/archive/p/ebookdroid/
	Google Code archive. Contains all versions we used.

	https://github.com/JustLoy77/ebookdroid
	Github repository. We're not sure if this is from the original
	developer or a fork, but it contains source code for all the
	versions we used.

AnkiDroid:
	https://github.com/ankidroid/Anki-Android
	Github repository. Contains all versions we used, including source.

Financius:
	https://github.com/mvarnagiris/financius-public
	Github repository. Contains all versions we used, including source.

K9Mail (UNUSED):
	https://code.google.com/archive/p/k9mail/
	Google Code archive. Contains all versions we used. Includes
	source code, but maybe not the right version.

	https://github.com/k9mail/k-9/
	Github repository. Contains source code for some versions.

SimpleCalendar
	https://github.com/SimpleMobileTools/Simple-Calendar
	Github repository. Contains all versions we used, including source.

AnExplorer
	https://github.com/1hakr/AnExplorer
	Github repository. Contains all versions we used, including source.
	
Minetest
	https://github.com/minetest/minetest
	Github repository. Contains all versions we used, including source.
	
	http://dev.minetest.net/Changelog
	Changelog for this application.
	
Materialistic
	https://github.com/hidroh/materialistic
	Github repository. Contains all versions we used, including source.
	Changelog in releases section.
	
---------------------------------------------------------------------

BUG REPORTS:
Shortyz - download_puzzles
	Debugging Energy-efficiency Related Field Failures in Mobile Apps
	Page 8 of this paper gives details about an energy bug found in
	Shortyz. The paper also gives an overview of how to fix it.

ConnectBot - connect_ssh
	https://github.com/connectbot/connectbot/issues/391
	Contains the bug report. Clicking on the commit, shows the fix.
	The app establishes an ssh connection with a given computer.
	It can either start a shell session with this connection or not.
	auto-download is apparently energy expensive and only needed
	when starting a shell session. The bug is that a relay thread
	was started regardless of whether a shell session was started.
	The fix is an if statement that checks if a shell session is open.
	
ConnectBot- ssh_shell
	Contains no energy bugs as far as we know. Used to
	detect energy differences between release versions.

EBookDroid - open_pdf
	https://code.google.com/archive/p/ebookdroid/issues/23
	Contains the bug report. Essentially the screen does not dim when
	the app has a file open. This was implemented as a feature, but
	it could be seen as a bug.

AnkiDroid - learn_cards
	https://github.com/ankidroid/Anki-Android/issues/951
	Contains the bug report. The bug is the lack of a day/night mode.
	Note the report says that the issue was not truly an issue. But
	they eventually did implement a day/night mode.
	
AnkiDroid - make_deck
	https://github.com/ankidroid/Anki-Android/issues/951
	Contains the bug report. The bug is the lack of a day/night mode.
	Note the report says that the issue was not truly an issue. But
	they eventually did implement a day/night mode.

Financius - write_transactions
	Contains no energy bugs as far as we know. Used to
	detect energy differences between release versions.

K9Mail (UNUSED) - polling
	https://code.google.com/archive/p/k9mail/issues/864
	Contains the bug report. Essentially the app polls too often for
	new mail. Fixed was the polling interval changing based on time
	and location. The fixed was introduced through the use of a
	plugin. We could not get the plugin to function correctly. But
	they later implemented a way of changing the polling interval, but
	it have only a couple options.

SimpleCalendar - make_events
	Contains no energy bugs as far as we know. Used to check
	no energy differences between commit level versions.
	
AnExplorer - explore_files
	Probably contains an energy bug, but we have no clue what it is.
	
Minetest - play_game
	Contains no energy bugs as far as we know. Used to
	detect energy differences between release versions.
	
Materialistic - read_article
	Contains no energy bugs as far as we know. Used to
	detect energy differences between release versions.

---------------------------------------------------------------------

SCENARIOS CONSIDERED:
Shortyz:
	Name:
		download_puzzles
	Preconfigured settings:
		Set auto-download off.
		Set to delete 2+ days old puzzles on clean up
		Set screen to stay awake.
	Scenario:
		1. Start at home screen. Wait 5 seconds.
		2. Touch Shortyz app. Touch green download button on bottom right.
		3. Scroll through dates until you reach Mar 09, 2017. Wait 5 
		   seconds.
		4. Touch download. Wait 10 seconds.
		5. Repeat steps 2-5 using the date Mar 10, 2017.
		6. Close out of application using app switcher.

ConnectBot:
	Name:
		connect_ssh
	Preconfigured settings:
		Set connection to close when app is in background.
		Set not to start shell session.
		Have it previously connected to a computer.
		Set screen to stay awake.
	Scenario:
		1. Start at home screen. Wait 5 seconds.
		2. Touch ConnectBot app. Touch saved connection.
		3. Enter password.
		4. Wait 30 seconds.
		5. Close the app using the app switcher.
	Notes: Each time the app is reinstalled (whenever it switches versions), 
	you must manually click through the installation welcome screen.
		
	Name:
		ssh_shell
	Preconfigured settings:
		Set connection to close when app is in background.
		Set to start shell session.
		Have it previously connected to a computer.
		Set screen to stay awake.
	Scenario:
		1. Start at home screen. Wait 5 seconds.
		2. Touch ConnectBot app. Touch saved connection.
		3. Enter password.
		4. Execute command: "ls"
		5. Execute command: "mkdir test"
		6. Execute command: "cd test"
		7. Execute command: "printf "text">test.txt
		8. Execute command: "cd .."
		9. Execute command: "rm -r test"
		10. Close the connection.
		11. Close the app using the app switcher.

EBookDroid
	Name:
		open_pdf
	Preconfigured settings:
		Set screen time out to 15 seconds.
		Set in app not to stay awake.
		Have a pdf set up to open.
	Scneario:
		1. Start at home screen. Wait 5 seconds.
		2. Touch EBookDroid and open a pdf.
		3. Wait 15 seconds. Screen will time out. Wait 10 seconds.
		4. Force-stop app through ADB.

AnkiDroid
	Name:
		learn_cards
	Preconfigured settings:
		Set screen to stay awake
		On fixed versions turn on night mode.
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Touch Ankidroid and touch a deck.
		3. Play through exactly 5 cards at an even pace, randomly
			 selecting "hard" and "good" options as you go.
		4. Close the app using the app switcher.
		5. Wait on the home screen for 5 seconds.

AnkiDroid
	Name:
		make_deck
	Preconfigured settings:
		Set screen to stay awake
		On fixed versions turn on night mode.
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Touch Ankidroid and touch a deck.
		3. Create a deck.
		4. Add 5 cards to the deck.
		5. Go back to the deck menu.
		6. Delete the newly made deck.
		7. Close the app using the app switcher.

Financius
	Name:
		write_transactions
	Preconfigured settings:
		Create an account before gathering data. Perform a transaction as 
		well.
		Set screen to stay awake.
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Touch Financius.
		3. Touch the plus button. Make a transaction by putting in an
			 amount and touching the equals sign.
		4. Set a category for the transaction. Confirm and save it.
		5. Repeat steps 3-4 with different details.
		6. Wait 1 second. Close the app using the app switcher.
		7. Wait on the home screen for 5 seconds.

K9Mail (UNUSED)
	Name:
		polling
	Preconfigured settings:
		Set screen to stay awake.
		Set polling interval depending on version. 1 minute for buggy 
		versions. Greater for fixed versions.
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Open K9Mail.
		3. Put it into the background by pressing the home button.
		4. Wait 3 minutes and 30 seconds.
		5. Open K9Mail and then close it using the app switcher.
		
		
SimpleCalendar
	Name:
		make_events
	Preconfigured settings:
		Set screen to stay awake.
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Open SimpleCalendar.
		3. Go to month November.
		4. Press any day.
		5. Make a new event.
		6. Repeat steps 4-5 two more times.
		7. Change the view to the simple event list.
		8. Close the app through the app switcher.
		9. Make sure to clear all data for the app!
		
AnExplorer
	Name:
		explore_files
	Preconfigured settings:
		Be wary of the popup that appears occasionally for rating the app.
		Set screen to stay awake.
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Open AnExplorer.
		3. Press Internal Storage button.
		4. Go to Downloads folder.
		5. Scroll down through the folder.
		6. Scroll back up through the folder.
		7. Select a file and view its info,
		7. Compress the same file.
		8. Delete the resulting archive.
		9. View the current processes.
		10. Close the app using the app switcher.
		
Minetest
	Name:
		play_game
	Preconfigured settings:
		Have a single player world pre-created with the player set to an
		area that is easy to move in (flat ground). Furthermore, create 
		an environment in this area where the scenario can be executed 
		repeatedly (two 3x3 dirt walls).
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Open Minetest.
		3. Start the singleplayer session.
		4. Move forward until the dirt wall is within arms reach.
		5. Break the
		 top two blocks in the center column.
		6. Replace the broken blocks with dirt again.
		7. Jump three times.
		8. Repeat steps 5 and 6
		10. Move backward until a dirt wall blocks the player.
		12. Exit the app to the menu.
		13. Close the app using the app switcher.
		
Materialistic
	Name:
		read_article
	Preconfigured settings:
		Set screen to stay awake.
	Scenario:
		1. Start on home screen. Wait 5 seconds.
		2. Open Materialistic.
		3. Scroll down the list of articles.
		4. Scroll back up the list of articles.
		5. Search with the following string: "housing a prisoner"
		6. Open the first result (Housing a prisoner in California costs
		   more than a year at Harvard).
		7. Read through the article.
		8. Press comments tab.
		9. Read through the comments.
		10. Go back.
		11. View the user information.
		12. Go back.
		13. Close the app using the app switcher.
		
---------------------------------------------------------------------

VERSIONS CONSIDERED:

Shortyz - download_puzzles
	4.0.0 - Buggy
	4.0.2 - Buggy
	4.0.5 - Buggy
	4.0.7 - Fixed. We fixed it ourselves.

ConnectBot - connect_ssh
	1.8.3 - Buggy
	1.8.6 - Buggy
	1.8.6 - Fixed. We fixed 1.8.6 ourselves.
	1.9.0a2 - Fixed by developers.
	
ConnectBot - ssh_shell
	1.8.3
	1.8.4
	1.8.5
	1.8.6

EBookDroid - open_pdf
	1.1.0 - Buggy
	1.1.2 - Buggy
	1.1.4 - Fixed by developers.
	1.2.0 - Fixed by developers.

AnkiDroid - learn_cards
	2.5 - Buggy
	2.6 - Buggy
	2.7 - Fixed by developers.
	2.8 - Fixed by developers.
	
AnkiDroid - make_deck
	2.5 - Buggy
	2.6 - Buggy
	2.7 - Fixed by developers.
	2.8 - Fixed by developers.

Financius - write_transactions
	0.12.1
	0.13.0
	0.14.0
	0.15.0

K9Mail (UNUSED) - polling
	1.993 - Buggy
	2.000 - Buggy
	2.391 - Fixed by developers.
	2.505 - Fixed by developers.

SimpleCalendar - make_events
	8003feb
	5379244
	b2fe795
	fd2ec79

AnExplorer - explore_files
	e7e51f8
	419112b
	53086f3
	d9a7443
	
Minetest - play_game
	0.4.12 
	0.4.13 
	0.4.14 
	0.4.15
	
Materialistic - read_article
	57
	58
	59
	60

