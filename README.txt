10 May 2018

Delete all_apks.txt, NEW make_test_data.py, NEW testing, NEW data, NEW Orientation.txt

8 May 2018

Further restructuring:
  -"performance-test_all.sh" has been moved from backend/scripts to set-up (this may change again)
  -the directory output has been deleted and its contents have been moved to analysis
  -"bar_chart.R" is now "format_barchart.R"
  -NEW "barchart.R" has been added to backend/scripts/analysis

2 May 2018

The project is being restructured. It is not completely finished; there is a brief
summary of movement so far below. Many of the scripts have NOT had the paths they use
updated, and they won't work until their paths are correct.

* - a star indicates that the contents/structure of a directory have changed

Original contents of project root directory:
  - Directories: all_apks*, app_apks*, analysis, help_files*, notes, replays*, scripts*
  - Files: expectations.txt, experiment_analysis.csv, experiment_input.csv,
	make_folder.sh, manual_plot.R, README.txt, performance_test-all.sh

New contents of project root directory:
  - Directories: backend, output, set-up
  - Files: README.txt

[proj] = project root directory (the directory this file is in)
[app] = app name
[scen] = scenario name
[var] = variation level (0, 1, 5, or 10 secs)
[ver] = app version
[dist] = distance measure 

{filename} = a filename in curly brackets indicates that the directory may have multiple 
  files of the form provided

Details:

  backend
    the directory all_apks has been deleted, and its contents (all_apks.txt) have been 
      moved to [proj]/backend
    the directory app_apks has been moved from [proj]/app_apks to [proj]/backend/app_apks; 
      its structure has been changed from app_apks/[app]/[scen]/{[app]-[ver].apk} to
      app_apks/{[app]-[ver].apk}
    the directory help_files has been deleted, and its contents have been moved to
      [proj]/backend/documentation
    the directory notes has been moved from [proj]/notes to [proj]/backend/documentation/notes
    the directory replays has been moved from [proj]/replays to [proj]/backend/replays;
      its structure has been changed from replays/[app]/[scen]/recordedEvents.txt to 
      replays/{[scen]-recordedEvents.txt}
    the directory scripts has been moved from [proj]/scripts to [proj]/backend/scripts;
      its structure has been changed extensively.

      Original contents of scripts: 
        - Directories: algo_scripts, deprecated, helper_scripts, misc*, RERAN_Modifications
        - Files: run_calc_energies.R, run_diff_calcs.R, run_analysis.R, run_summarize.R,
            run_plotting.R
      New contents of scripts:
        - Directories: analysis, misc, preprocessing
        - Files: performance-run-replay.py, performance_test-all.sh
      analysis
        NEW bar_chart.R has been added to analysis
        the five files in scripts originally have been moved to scripts/analysis
        the directory helper_scripts has been moved from scripts/helper_scripts to
          scripts/analysis/helper_scripts
        the directory algo_scripts has been moved from scripts/algo_scripts to
          scripts/analysis/helper_scripts/algo_scripts
      misc
        the directory deprecated has been moved from scripts/deprecated to 
          scripts/misc/deprecated
        the directory RERAN_Modifications has been moved from scripts/RERAN_Modifications
          scripts/misc/RERAN_Modifications
        the files make_folders.sh and manual_plot.R have been moved from [proj] to 
          scripts/misc
      preprocessing
        NEW Readme.txt has been added to preprocessing
        the files check_errors.py, file_cut.py, and log_to_csv.sh have been moved from
          scripts/misc to scripts/preprocessing
      the file performance-run-replay.py has been moved from scripts/misc to scripts
      the file performance_test-all.sh has been moved from [proj] to scripts

  output
    analysis has moved from [proj]/analysis to [proj]/output/analysis
    experiment_analysis.csv has moved from [proj]/experiment_analysis.csv to 
      [proj]/output/experiment_analysis.csv
    
  set-up
    expectations.txt has moved from [proj]/expectations.txt to 
      [proj]/set-up/expectations.txt
    experiment_input.csv has moved from [proj]/experiment_input.csv to 
      [proj]/set-up/experiment_input


######## Contents of document before 2 May 2018 below ########

## What Cagri needs to do:

Gather power-traces for 10 seconds of variation for the following subjects.
List of subjects can be found in experiment_input.csv.

1. ConnectBot, ssh_shell
2. AnkiDroid, make_deck
3. AnExplorer, explore_files
4. Materialistic, read_article

---------------------------------------------------------------

## How to gather power-traces for a single subject:

Let us assume we are gathering power-traces for ConnectBot, ssh_shell.
For these experiments, we always do one subject at a time rather than multiple 
ones.

#### First we set up the phone:

1. Turn on power supply. Don't change settings.

2. Press off/on button.

3. Turn on phone.

4. All phone settings should be kept as is (don't turn wifi off or anything).

5. Plug USB cable into the phone and ensure ADB is connected.

6. Connect ADB over Wifi. This involves two commands:
			> adb tcpip 5555
			> adb connect IP_ADDRESS
	 You can find the phones IP_ADDRESS by going into Settings/About phone/Status.
	 Note that the IP_ADDRESS is dynamic so it may change from time to time.
	 
7. Disconnect the USB cable from the phone.

8. Place the subject app on the homescreen in the lower left corner.
   In our case, we will place ConnectBot there. Wherever ConnectBot is
   on the home screen is where all subjects apps must be placed.
   
9. Do certain app configurations if necessary. The exact configuration details
   be found in help_files/app_info.txt under the "SCENARIOS CONSIDERED" 
   heading and "Preconfigred settings" subheading. YOU WILL NEED TO change these
   settings at some point.
   
#### Next we configure performance_test-all.sh

1. Read through the comments. They should provide most needed information.

2. Set TRIAL_COUNT to 30. Sometimes the measuring fails in the middle of an 
	 execution, and we have to restart everything. In order to avoid redoing 
	 trials, we use the CORRECTION_VAL variable. It is set to the number of 
	 sucessful trials. Remember to adjust TRIAL_COUNT accordingly. For example if 
	 we have 5 out of 30 successful trials, we would have:
	 		TRIAL_COUNT=25
	 		CORRECTION_VAL=5

3. Uncomment the REPLAYS assignment statement for the subject. For the case of 
   ConnectBot, ssh_shell, we uncomment:
			REPLAYS="/home/greense/Projects/GTHA_project/GTHA_project_data/replays/
			ConnectBot/ssh_shell/recordedEvents.txt,ConnectBot,ssh_shell"
	 Keep all other assignment statements commented.
	 
4. Uncomment the VERSIONS assignment statements for the subject. For our case
	 we uncomment:
	 		VERSIONS[0]="1.8.3"
			VERSIONS[1]="1.8.4"
			VERSIONS[2]="1.8.5"
			VERSIONS[3]="1.8.6"
	 This means that we will gather 30 power-traces for all 4 versions considered.
	 Sometimes, we only want to gather traces for a single or few versions. We 
	 usually do this when some problem occurs while recording. In this case just 
	 reassign indices and comment statements as needed. For example if we only 
	 wanted to gather traces for version 1.8.6, we would have:
	 		#VERSIONS[0]="1.8.3"
			#VERSIONS[1]="1.8.4"
			#VERSIONS[2]="1.8.5"
			VERSIONS[0]="1.8.6"
			
5. Uncomment the APKNAMES assignment statements for the subject. NOTE that this
	 should always mirror the statements for VERSIONS. Again if we only wanted to
	 gather traces for version 1.8.6 of Connectbots, we would have:
	 		#APKNAMES[0]="ConnectBot/ssh_shell/ConnectBot-1.8.3.apk"
			#APKNAMES[1]="ConnectBot/ssh_shell/ConnectBot-1.8.4.apk"
			#APKNAMES[2]="ConnectBot/ssh_shell/ConnectBot-1.8.5.apk"
			APKNAMES[0]="ConnectBot/ssh_shell/ConnectBot-1.8.6.apk"
			
6. Uncomment the APKPACKAGES assignment statements for the subject. This should 
	 also mirror the statements for VERSIONS.
	 
7. All other lines in the file should be kept the same.

#### Now we run the experiment command

With a terminal open in the project directory, run the following command

		> ./performance_test-all.sh
		
Now we wait for everything to be done. Make sure to check every now and then to
see if some error occurs. If so, make sure to press CTRL-c in the terminal 
window and then close it. This should completely kill off related processes.
The traces are saved into the raw_data folder. After you gather the data for
a subject, IT WOULD BE WISE to execute raw_data/check_errors.py. This script
will look at the data and find some errors. I think for one of the apps, many 
errors were found in the trials, which needed to be redone. Consult Gifan, if 
you need to know what these errors means.

Good luck!



	 

